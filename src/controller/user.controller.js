let _userService=null
class UserController{
    constructor({UserService}){
        _userService=UserService
    }
    async get(req,res){
        const {userId}=req.params
        const user = await _userService.get(userId)
        return res.send(user)
        
    }
    async getAll(req,res){
        const user = await _userService.getAll()

        return res.send(user)
    }
    async delete(req,res){
        const {userId}=req.params
        const user = await _userService.delete(userId)
        return res.send(user)
    }
    async update(req,res){
        const body=req.body
        const {userId}=body
        const user = await _userService.update(body,userId)
        return res.send(user)
    }

    async update(req,res){
     
        const body=req.body
        const userId=req.params._id
        const user = await _userService.update(body,userId)
        return res.send(user)
    }

    
}
module.exports=UserController
