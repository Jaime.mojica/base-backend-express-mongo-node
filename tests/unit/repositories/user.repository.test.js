const {UserRepository} =require('../../../src/repositories')
const mokingoose  =require('mockingoose').default
const {UserModel}  =require('../../../src/models')
let {UserModelMock:{user,users}
} =require('../../mocks')
const { default: mockingoose } = require('mockingoose')

describe('User repository test',()=>{

    beforeEach(()=>{
        mockingoose.resetAll()
        jest.clearAllMocks()
    })

    it('Should return a user by id',async ()=>{
        const _user= {...user}
        delete _user.password
        mockingoose(UserModel).toReturn(user,"findOne")

        const _userRepository=new UserRepository({UserModel})
        const expectes = await _userRepository.get(_user._id)
        
        expect(JSON.parse(JSON.stringify(expectes))).toMatchObject(_user)
    })

    it('Should return a user by username',async ()=>{
        const _user= {...user}
        delete _user.password
        mockingoose(UserModel).toReturn(user,"findOne")

        const _userRepository=new UserRepository({UserModel})
        const expectes = await _userRepository.getUserByUsername(_user.username)
        
        expect(JSON.parse(JSON.stringify(expectes))).toMatchObject(_user)
    })

    it('Should return all the users',async ()=>{
        users=users.map(uu=>{
            delete uu.password
            return uu
        })
        mockingoose(UserModel).toReturn(users,"find")
        const _userRepository=new UserRepository({UserModel})
        const expectes = await _userRepository.getAll()
        expect(JSON.parse(JSON.stringify(expectes.coleccion))).toMatchObject(users)
    })


    it('Should update a user by id',async ()=>{
        const _user= {...user}
        //_user.name="copy"
        delete _user.password
        _user.name="copy"
        mockingoose(UserModel).toReturn(_user,"findOneAndUpdate")
        
        const _userRepository=new UserRepository({UserModel})
        const expectes = await _userRepository.update(user._id, {
            name: "copy"
          });
        expect(JSON.parse(JSON.stringify(expectes))).toMatchObject(_user)
    })

    it('Should delete a user by id',async ()=>{
        const _user= {...user}
        //_user.name="copy"
        delete _user.password
        _user.active=false
        mockingoose(UserModel).toReturn(_user,"findOneAndUpdate")
        
        const _userRepository=new UserRepository({UserModel})
        const expectes = await _userRepository.update(user._id, {
            active:false
          });
        expect(JSON.parse(JSON.stringify(expectes))).toMatchObject(_user)
    })
})
