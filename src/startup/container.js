const {asClass,asFunction,asValue,createContainer} = require('awilix')
//config
const config=require('../config')
const app = require('.')
//Routes
const {UserRoutes,AuthRoutes,} = require('../routes/index.routes')
const Routes = require('../routes')
//Controllers
const { UserController,EdtController,AuthController,}=require('../controller')
//Services
const {UserService,AuthService,} = require('../services')
//Repositories
const {UserRepository,}= require('../repositories')
//Models
const {UserModel, } =require('../models')

const container= createContainer()

container.register({
    router:asFunction(Routes).singleton(),
    app:asClass(app).singleton(),
    config:asValue(config)
})
//Registering the models
.register({
    UserModel:asValue(UserModel),
})
//Registering the Services
.register({
    UserService:asClass(UserService).singleton(),
    AuthService:asClass(AuthService).singleton(),
})
//Registering the Repositories
.register({
    UserRepository:asClass(UserRepository).singleton(),
})
.register({
    UserRoutes:asFunction(UserRoutes).singleton(),
    AuthRoutes:asFunction(AuthRoutes).singleton(),
})
.register({
    UserController:asClass(UserController.bind(UserController)).singleton(),
    AuthController:asClass(AuthController.bind(AuthController)).singleton(),
})

module.exports=container