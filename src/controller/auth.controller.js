
let _authService=null

class AuthController{
 constructor({AuthService}){

    _authService=AuthService
 }

 async singin(req, res){
    
    
    const {body} = req
    const creds= await _authService.singin(body)
    return res.status(200).send(creds)
 }
 async singup(req, res){
    const {body} = req
    
    const newUser= await _authService.singup(body)
    return res.status(200).send(newUser)
 }
}
module.exports=AuthController