# Base-Backend-Express-mongo-Node

Layer architecture with testing and routing


clone the project in your device:
git clone https://gitlab.com/jaime.m/base-backend-express-mongo-node.git
then.
cd base-backend-express-mongo-node.git

install npm dependecies with 
npm install

then you have to create .env file with the required configuration with your parameters

- `.env`

  ```json
  {
    PORT: Application port
    MONGO_URI": URL of mongo database
    APPLICATION_NAME:name of your application
    JWT_SECRET=SomeSecretKey
    CACHE_KEY=AnotherSecretKey
    
  }
  ```
  
  then you can use the authentication and the authorization functionality of this backend