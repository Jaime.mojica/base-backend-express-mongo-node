const mongoose = require('mongoose')
const {Schema}=mongoose
const {compareSync,hashSync,genSaltSync} = require('bcryptjs')

const UserSchema= new Schema({
    username:{type:String,required:true, unique:true},
    name:{type:String, required:true},
    password:{type:String, required:true},
    role:{type:String, required:true,enum:["ADMIN_ROLE","USER_ROLE"]},
    active:{type:Boolean,required:true}
},{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

//Elimina la contraseña en el envío de información
UserSchema.methods.toJSON= function (){
    let user = this.toObject()
    delete user.password
    return user
}

UserSchema.pre('save',async function(next){
    const user=this
    
    if (!user.isModified('password')){
        return next()
    }
        const salt = genSaltSync(10)
        const hashedpassword=hashSync(user.password,salt)
        user.password=hashedpassword
        next()
    
})
UserSchema.methods.comparePasswords =function(password){
    return compareSync(password,this.password)
}

module.exports= mongoose.model('user',UserSchema)